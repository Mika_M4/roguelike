﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
public class GameController : MonoBehaviour {
    public static GameController Instance;
    public bool isPlayerTurn;
    public bool areEnemiesMoving;
    public int playerCurrentHealth = 50;
    private List<Enemy> enemies;
    private GameObject levelImage;
    private Text levelText;
    private BoardController boardController;
    private int secondsUntilLevelStart = 2;
    private int currentLevel = 1;
    private bool settingUpGame;
    public AudioClip gameOverSound;
    public Difficulty currentDifficulty = Difficulty.Easy;
	void Awake () {
        if (Instance != null && Instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
        boardController = GetComponent<BoardController>();
        enemies = new List<Enemy>();
	}
    void Start()
    {
        //InitializeGame();
    }	
    public void StartGame()
    {
        DisableStartScreen();
        InitializeGame();
    }
    private void InitializeGame()
    {
        settingUpGame = true;
        levelImage = GameObject.Find("Level Image");
        levelText = GameObject.Find("Level Text").GetComponent<Text>();
        levelText.text = "Day " + currentLevel;
        levelImage.SetActive(true);
        enemies.Clear();
        boardController.SetupLevel(currentLevel);
        Invoke("DisableLevelImage", secondsUntilLevelStart);
    }
    private void DisableLevelImage()
    {
        levelImage.SetActive(false);
        isPlayerTurn = true;
        areEnemiesMoving = false;
        settingUpGame = false;
    }
    private void DisableStartScreen()
    {
        GameObject.Find("StartScreen").SetActive(false);
    }
    private void OnLevelWasLoaded(int levelLoaded)
    {
        currentLevel++;
        DisableStartScreen();
        InitializeGame();
    }
	void Update () {
        if (isPlayerTurn || areEnemiesMoving || settingUpGame)
        {
            return;
        }
        StartCoroutine(MoveEnemies());
	}
    private IEnumerator MoveEnemies()
    {
        areEnemiesMoving = true;

        yield return new WaitForSeconds(0.2f);
        foreach (Enemy enemy in enemies)
        {
            enemy.MoveEnemy();
            yield return new WaitForSeconds(enemy.moveTime);
        }
        areEnemiesMoving = false;
        isPlayerTurn = true;
    }
    public void AddEnemyToList(Enemy enemy)
    {
        enemies.Add(enemy);
    }
    public void GameOver()
    {
        isPlayerTurn = false;
        SoundController.Instance.music.Stop();
        SoundController.Instance.PlaySingle(gameOverSound);
        levelText.text = "You starved after " + currentLevel + " day" + ( currentLevel > 1 ? "s" : "" ) + "...";
        levelImage.SetActive(true);
        enabled = false;
        Invoke("StartOver", 2);
    }
    public void StartOver()
    {
        currentLevel = 0;
        GameObject.Find("Player").GetComponent<Player>().SetHealth(50);
        enabled = true;
        isPlayerTurn = true;
        Application.LoadLevel(0);
    }
    public void DragDifficulty()
    {
        Slider slider = GameObject.Find("Slider").GetComponent<Slider>();
        string difficulty;
        difficulty = slider.value == 0 ? "No Monsters" : (slider.value == 1 ? "Easy" : (slider.value == 2 ? "Medium" : "Hard"));
        Text t = GameObject.Find("difficulty").GetComponent<Text>();
        currentDifficulty = (Difficulty)slider.value;
        t.text = "Difficulty: " + difficulty;
    }
}
public enum Difficulty
{
    NoMonster = 0,
    Easy = 1,
    Medium = 2,
    Hard = 3
}