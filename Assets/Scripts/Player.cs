﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Player : MovingObject {
    private Animator animator;
    private int attackPower = 1;
    private int playerHealth = 50;
    public Text healthText;
    public AudioClip movementSound1;
    public AudioClip movementSound2;
    private int healthPerFruit = 5;
    private int healthPerSoda = 10;
    private int healthPerBurger = 2;
    private int secondsUntilNextLevel = 1;
    public AudioClip chopSound1;
    public AudioClip chopSound2;


    public AudioClip fruitSound1;
    public AudioClip fruitSound2;
    public AudioClip sodaSound1;
    public AudioClip sodaSound2;
    
    protected override void Start()
    {
        base.Start();
        animator = GetComponent<Animator>();
        playerHealth = GameController.Instance.playerCurrentHealth;
        healthText.text = "Health: " + playerHealth;
    }
    private void OnDisable()
    {
        GameController.Instance.playerCurrentHealth = playerHealth;
    }
	void Update () {
        if (!GameController.Instance.isPlayerTurn)
            return;
        CheckIfGameIsOver();
        int xAxis = 0, 
            yAxis = 0;
        xAxis = (int)Input.GetAxisRaw("Horizontal");
        yAxis = (int)Input.GetAxisRaw("Vertical");
        if (xAxis != 0)
        {
            yAxis = 0;
        }
        if (xAxis != 0 || yAxis != 0)
        {
            playerHealth--;
            
            healthText.text = "Health: " + playerHealth;
            if (xAxis == 1)
            {
                Flip(Rotation.Right);
            }
            else if (xAxis == -1) {
                Flip(Rotation.Left);
            }
            SoundController.Instance.PlaySingle(movementSound1, movementSound2);
            Move<Wall>(xAxis, yAxis);
            GameController.Instance.isPlayerTurn = false;
        }
        
	}
   
    private void OnTriggerEnter2D(Collider2D objectPlayerCollidedWith)
    {
        switch (objectPlayerCollidedWith.tag)
        {
            case "Exit":
                Invoke("LoadNewLevel", secondsUntilNextLevel);
                enabled = false;
                break;
            case "Fruit":
                UpdatePlayerHealth(healthPerFruit);
                objectPlayerCollidedWith.gameObject.SetActive(false);
                SoundController.Instance.PlaySingle(fruitSound1, fruitSound2);
                break;
            case "Burger":
                UpdatePlayerHealth(healthPerBurger);
                objectPlayerCollidedWith.gameObject.SetActive(false);
                SoundController.Instance.PlaySingle(fruitSound1, fruitSound2);
                break;
            case "Soda":
                UpdatePlayerHealth(healthPerSoda);
                objectPlayerCollidedWith.gameObject.SetActive(false);
                SoundController.Instance.PlaySingle(sodaSound1, sodaSound2);
                break;
        }
    }
    protected override void HandleCollision<T>(T component)
    {
        Wall wall = component as Wall;
        animator.SetTrigger("playerAttack");
        SoundController.Instance.PlaySingle(chopSound1, chopSound2);
        wall.DamageWall(attackPower);
    }
    public void SetHealth(int health)
    {
        playerHealth = health;
    }
    public void TakeDamage(int damageReceived)
    {
        UpdatePlayerHealth(-damageReceived);
        animator.SetTrigger("playerHurt");
    }
    private void CheckIfGameIsOver()
    { 
        if (playerHealth <= 0)
        {
            GameController.Instance.GameOver();
        }
    }
    private void LoadNewLevel()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
    private void UpdatePlayerHealth(int value)
    {
        playerHealth += value;
        healthText.text = (value < 0 ? "-" : "+") + Mathf.Abs(value) + " Health\n" + "Health: " + playerHealth;
    }
}
