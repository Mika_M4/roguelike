﻿using UnityEngine;
using System.Collections;

public class Enemy : MovingObject {

    private Transform player;
    private bool skipCurrentMove;
    public bool isEnemyStrong;
    private Animator animator;
    public int attackDamage;
    public AudioClip enemySound1;
    public AudioClip enemySound2;

	protected override void Start () {
        GameController.Instance.AddEnemyToList(this);
        player = GameObject.FindGameObjectWithTag("Player").transform;
        animator = GetComponent<Animator>();
        skipCurrentMove = true;
        base.Start();
	}

	public void MoveEnemy()
    {
        if (skipCurrentMove)
        {
            if (isEnemyStrong)
            {
                int chanceToMove = Random.Range(1, 4);
                if (chanceToMove > 1)
                {
                    skipCurrentMove = false;
                    return;
                }
            }
            else
            {
                skipCurrentMove = false;
                return;
            }
        }
        int xAxis = 0,
            yAxis = 0;

        float xAxisDistance = Mathf.Abs(player.position.x - transform.position.x);
        float yAxisDistance = Mathf.Abs(player.position.y - transform.position.y);
        if (xAxisDistance > yAxisDistance)
        {
            if (player.position.x > transform.position.x)
            {
                xAxis = 1;
                Flip(Rotation.Left);
            }
            else
            {
                xAxis = -1;
                Flip(Rotation.Right);
            }
        }
        else
        {
            yAxis = (player.position.y > transform.position.y ? 1 : -1);
        }
        Move<Player>(xAxis, yAxis);
        skipCurrentMove = true;
	}
    protected override void HandleCollision<T>(T component)
    {
        Player player = component as Player;
        int playerX = (int)player.transform.position.x;
        int enemyX = (int)transform.position.x;
        if (playerX > enemyX)
        {
            Flip(Rotation.Left);
        }
        else
        {
            Flip(Rotation.Right);
        }
        player.TakeDamage(attackDamage);
        animator.SetTrigger("enemyAttack");
        SoundController.Instance.PlaySingle(enemySound1, enemySound2);
    }
    

}
